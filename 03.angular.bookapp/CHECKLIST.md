# Basic Evaluation Check List

Before you submit a project for evaluation, make sure ...

1. It is linted for all languages in the project
2. It has README which explains 

   - what the project does
   - how to build and run the project 
   
