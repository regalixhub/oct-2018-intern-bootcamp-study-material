# October 2018, Bootcamp study material

The following lists the study material prescribed for Oct 2018 boot-camp to in which we 
train fresh college grads in full stack programming.

## Tools

1. Unix Command prompt starter
   https://in.udacity.com/course/linux-command-line-basics--ud595

2. Git and GitLab
   https://in.udacity.com/course/how-to-use-git-and-github--ud775-india


## HTML / CSS

1. HTML5 / CSS 3 basics
   https://classroom.udacity.com/courses/ud001-india

## Basic JavaScript

1. https://classroom.udacity.com/courses/ud803-india

## Basic TypeScript

1. https://www.youtube.com/watch?v=YPShu0H3RbM&list=PLqq-6Pq4lTTanfgsbnFzfWUhhAz3tIezU

## Basic Angular.io

1. https://www.youtube.com/playlist?list=PLC3y8-rFHvwhBRAgFinJR8KHIrCdTkZcZ


## Basic Spring Boot

https://www.youtube.com/playlist?list=PLqq-6Pq4lTTbx8p2oCgcAQGQyqN8XeA1x

Please Note:

1. This is a rather old video series.
2. Though the a lot of concepts have moved on, the basics is still solid. So follow along 
    what the tutor presents.
3. For your assignment you will be using the latest template from spring initializr.

